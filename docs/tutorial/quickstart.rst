==========
Quickstart
==========

In this quickstart, you'll learn how to create a basic Idem plugin.

**Note**: It is recommended that you use a Python virtual environment when creating a new Idem provider plugin.

Create a virtual environment
++++++++++++++++++++++++++++

Before you start, ensure that you installed Python 3.8 or later. If you are running 3.7 or earlier, you might need to use `python3` instead of `python` in the commands in the rest of this tutorial.

To verify your Python version, run the following command:

.. code-block:: bash

    python -V

Next, create your virtual environment:

.. code-block:: bash

    python -m venv env
    source env/bin/activate

Now you should be in your new Python virtual environment.

Update pip
++++++++++

Next, update to the latest version of `pip` inside your virtual environment:

.. code-block:: bash

    pip install -U pip


Install dependencies
++++++++++++++++++++

Next, you need to install `idem` and `pop-create-idem`:

.. code-block:: bash

    pip install idem
    pip install pop-create-idem

You now have access to the `pop-create` command for creating Idem plugins.


Generate a skeleton Idem Cloud plugin
+++++++++++++++++++++++++++++++++++++

To generate a new skeleton Idem Cloud plugin, run the following command:

.. code-block:: bash

    pop-create idem-cloud --directory /path/to/new/project --project-name=idem-{my_cloud} --simple-service-name={my_cloud} --author={company_name}


`pop-create idem-cloud` creates an empty project with the directory structure needed to get started with your Idem Cloud plugin.

Next, you need to add exec and state modules to your project. See the `Create Provider Plugin_` tutorial to learn how to create exec, state, and tool modules for your project.

.. _Create Provider Plugin: https://docs.idemproject.io/idem/en/latest/developer/tutorials/create-provider-plugin/create-provider-plugin.html

The directory tree now looks like this:

.. code-block:: bash

    .
    ├── CONTRIBUTING.rst
    ├── LICENSE
    ├── README.rst
    ├── __pycache__
    │   └── noxfile.cpython-311.pyc
    ├── build.conf
    ├── docs
    │   ├── conf.py
    │   ├── index.rst
    │   ├── releases
    │   │   ├── 0.1.0.rst
    │   │   └── index.rst
    │   ├── sitevars.rst
    │   ├── topics
    │   │   ├── contributing.rst
    │   │   └── license.rst
    │   └── tutorial
    │       ├── example.rst
    │       └── index.rst
    ├── example
    │   ├── absent
    │   │   └── init.sls
    │   └── present
    │       └── init.sls
    ├── {project-name}
    │   ├── acct
    │   │   ├── contracts
    │   │   └── simple_service_name
    │   │       ├── basic_auth.py
    │   │       └── default_auth.py
    │   ├── autogen
    │   │   └── simple_service_name
    │   │       └── templates
    │   │           └── sample.jinja2
    │   ├── cloudspec
    │   │   ├── contracts
    │   │   └── customize
    │   │       └── test-cloud.py
    │   ├── conf.py
    │   ├── exec
    │   │   ├── contracts
    │   │   └── simple_service_name
    │   │       ├── None
    │   │       │   ├── init.py
    │   │       │   └── recursive_contracts
    │   │       │       └── init.py
    │   │       ├── init.py
    │   │       └── sample.py
    │   ├── states
    │   │   ├── contracts
    │   │   └── simple_service_name
    │   │       ├── init.py
    │   │       └── sample.py
    │   ├── tool
    │   │   ├── contracts
    │   │   └── simple_service_name
    │   │       └── test_state_utils.py
    │   └── version.py
    ├── noxfile.py
    ├── requirements
    │   ├── base.txt
    │   ├── docs.txt
    │   ├── py3.10
    │   │   └── tests.txt
    │   ├── py3.11
    │   │   └── tests.txt
    │   ├── py3.8
    │   │   └── tests.txt
    │   ├── py3.9
    │   │   └── tests.txt
    │   └── tests.in
    ├── setup.cfg
    ├── setup.py
    └── tests
        ├── integration
        │   └── conftest.py
        └── unit
            └── conftest.py


Create an Idem Cloud plugin with a Swagger or OpenAPI3 specification
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`pop-create-idem` can auto generate an Idem Cloud plugin, including exec and state modules using a Swagger or OpenAPI3 specification.

To generate a new Idem plugin project with a Swagger specification, run the following command:

.. code-block:: bash

    pop-create swagger --directory /path/to/new/project --specification={swagger-spec-yaml-or-accessible-swagger-spec-json-url} --project-name=idem-{my_cloud} --simple_service_name={my_cloud} --author={company_name}

To generate a new Idem plugin project with an OpenAPI3 specification, run the following command:

.. code-block:: bash

    pop-create openapi3 --directory /path/to/new/project --specification={openapi3-spec-yaml-or-accessible-openapi3-spec-json-url} --project-name=idem-{my_cloud} --simple_service_name={my_cloud} --author={company_name}

To generate a new Idem plugin project with multiple Swagger or OpenAPI3 specifications, run the following command:

.. code-block:: bash

    pop-create swagger --directory /path/to/new/project --specification spec1.yaml spec2.yaml --project-name=idem-{my_cloud} --simple_service_name={my_cloud} --author={company_name}
    pop-create openapi3 --directory /path/to/new/project --specification spec1.yaml spec2.yaml --project-name=idem-{my_cloud} --simple_service_name={my_cloud} --author={company_name}

This command creates a new project with the boilerplate code needed to get started with each respective cloud provider.

Next, you need to configure exec, state, and tool modules before you can use them. The `how-to` directory contains a tutorial on how to configure an Idem plugin from a Swagger specification.

The directory tree now looks like this:

.. code-block:: bash

    .
    ├── CONTRIBUTING.rst
    ├── LICENSE
    ├── README.rst
    ├── build.conf
    ├── docs
    │   ├── conf.py
    │   ├── index.rst
    │   ├── releases
    │   │   ├── 0.1.0.rst
    │   │   └── index.rst
    │   ├── sitevars.rst
    │   ├── topics
    │   │   ├── contributing.rst
    │   │   └── license.rst
    │   └── tutorial
    │       ├── example.rst
    │       └── index.rst
    ├── example
    │   ├── absent
    │   │   ├── init.sls
    │   │   └── {my_cloud_resource}.sls
    │   └── present
    │       ├── init.sls
    │       └── {my_cloud_resource}.sls
    ├── {project-name}
    │   ├── __init__.py
    │   ├── acct
    │   │   ├── contracts
    │   │   └── {simple_service_name}
    │   │       ├── api_key_auth.py
    │   │       ├── basic_auth.py
    │   │       ├── client_credentials_auth.py
    │   │       └── default_auth.py
    │   ├── autogen
    │   │   └── {simple_service_name}
    │   │       └── templates
    │   │           ├── exec
    │   │           │   ├── create.jinja2
    │   │           │   ├── delete.jinja2
    │   │           │   ├── get.jinja2
    │   │           │   ├── list.jinja2
    │   │           │   └── update.jinja2
    │   │           ├── state
    │   │           │   ├── absent.jinja2
    │   │           │   ├── describe.jinja2
    │   │           │   └── present.jinja2
    │   │           ├── tests
    │   │           │   ├── exec
    │   │           │   │   ├── test_create.jinja2
    │   │           │   │   ├── test_delete.jinja2
    │   │           │   │   ├── test_get.jinja2
    │   │           │   │   ├── test_list.jinja2
    │   │           │   │   └── test_update.jinja2
    │   │           │   ├── states
    │   │           │   │   ├── test_absent.jinja2
    │   │           │   │   ├── test_describe.jinja2
    │   │           │   │   └── test_present.jinja2
    │   │           │   └── tool
    │   │           │       └── default.jinja2
    │   │           └── tool
    │   │               └── default.jinja2
    │   ├── cloudspec
    │   │   ├── contracts
    │   │   └── customize
    │   │       └── {simple_service_name}.py
    │   ├── conf.py
    │   ├── exec
    │   │   ├── contracts
    │   │   └── {simple_service_name}
    │   │       ├── init.py
    │   │       ├── {my_cloud_resource}.py
    │   │       └── v1_0_6
    │   │           ├── init.py
    │   │           └── recursive_contracts
    │   │               └── init.py
    │   ├── states
    │   │   ├── contracts
    │   │   └── {simple_service_name}
    │   │       ├── init.py
    │   │       └── {my_cloud_resource}.py
    │   ├── tool
    │   │   ├── contracts
    │   │   └── {simple_service_name}
    │   │       ├── session.py
    │   │       ├── test_state_utils.py
    │   │       └── {my_cloud_resource}.py
    │   └── version.py
    ├── noxfile.py
    ├── requirements
    │   ├── base.txt
    │   ├── docs.txt
    │   ├── py3.10
    │   │   └── tests.txt
    │   ├── py3.11
    │   │   └── tests.txt
    │   ├── py3.8
    │   │   └── tests.txt
    │   ├── py3.9
    │   │   └── tests.txt
    │   └── tests.in
    ├── setup.cfg
    ├── setup.py
    └── tests
        ├── integration
        │   ├── conftest.py
        │   ├── exec
        │   │   ├── __init__.py
        │   │   └── test_{my_cloud_resource}.py
        │   ├── states
        │   │   ├── __init__.py
        │   │   └── test_{my_cloud_resource}.py
        │   └── tool
        │       ├── __init__.py
        │       └── test_{my_cloud_resource}.py
        └── unit
            └── conftest.py

Installation
++++++++++++
Install your new project with `pip`. Your project needs to be added to the Python environment:

.. code-block:: bash

    pip install -e {project_root}

After installation, the new Idem Cloud plugin's exec and state modules are accessible to the pop `hub`.

Configuration
-------------
Create a `credentials.yml` file, encrypt it, and set environment variables:

.. code-block: yaml

        provider:
          profile_name:
            username: XXXXXXXXXXXX
            password: XXXXXXXXXXXX
            api_key: XXXXXXXXXXXXXXXXXXX

Next, use the `acct` command to encrypt this file using the fernet algorithm:

.. code-block:: bash

    $ idem encrypt credentials.yml
    YeckEnWEGOjBDVxxytw13AsdLgquzhCtFHOs7kDsna8=

    $ export ACCT_FILE={credentials.yml.fernet}
    $ export ACCT_KEY={key-generated-with-acct-encrypt}

See `Authenticating Idem with providers`_ for more information about the credentials file.

.. _Authenticating Idem with providers: https://docs.idemproject.io/getting-started/en/latest/topics/gettingstarted/authenticating.html

Running Idem commands
---------------------

You can run the `idem exec` command on a get function for a cloud resource:

.. code-block:: bash

    idem exec {simple_service_name}.{my_cloud_resource}.get resource_id={my_cloud_resource_identifier}

You can run the `idem state` command on a cloud resource:

.. code-block:: bash

    idem state my_state.sls

A sample SLS file might look like this:

.. code-block:: yaml

    {my_cloud_resource}_is_present:
      {simple_service_name}.my_cloud_resource.present:
      - name: {my_cloud_resource_name}
      - status: 'string'

See `Getting Started with Idem`_ for information about common Idem commands.

.. _Getting Started with Idem: https://docs.idemproject.io/getting-started

Next steps
++++++++++

* Try the example Swagger petstore tutorial :doc:`Auto-generate an Idem plugin from Swagger </tutorial/swagger_example>`, which walks you through generating an Idem plugin with a Swagger specification.
