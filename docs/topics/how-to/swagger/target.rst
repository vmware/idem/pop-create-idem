===================
Specify Target Path
===================

Sometimes it might be useful to generate an Idem provider plugin for a specific resource path or a subset of
resource paths in the Swagger/OpenAPI specs. Some examples include:

* When it takes a long time to generate a full Idem provider plugin because the Swagger/OpenAPI spec is large
* When a new resource type needs to be added under management with an existing Idem provider plugin

Target parameter
+++++++++++++++++

You can use the `--target` parameter to specify a single or multiple resources paths to be used to generate the Idem provider plugin:

.. code-block:: bash

    pop-create swagger --directory /path/to/new/project --specification={swagger-spec-yaml-or-accessible-swagger-spec-json-url} --project-name=idem-{my_cloud} --simple_service_name={my_cloud} --author={company_name} --target /my_cloud_resource/{resource_id}
