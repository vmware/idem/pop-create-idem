==========
Idem Cloud
==========

One advantage to using idem-cloud is that you can have a standardized CLI interface for every cloud. With `acct` profiles, credentials for accessing your cloud are encrypted. This means that you don't need plaintext credentials on your file system or in your shell history.

`idem-cloud` states work together between cloud providers and profiles. Your instance from one cloud provider can depend on the existence of a vpc from a completely different cloud provider.

In this document, you'll learn about the structure of an idem-cloud project.

Authenticating with idem-cloud
==============================

You can choose any authentication method. The authentication method must be interchangeable when you call exec modules.

For example, imagine that you want all of your plugins to authenticate with aiohttp. You want one plugin to handle username and password authentication, and another plugin to handle authentication with an API token. Both plugins create an aiohttp session, but they use credentials in different ways.

As another example, imagine that you want to have an account plugin that authenticates with a cookie, which is a different way of using an aiohttp session. On the back-end, however, it's the same, and it will be the same to exec modules that are calling the API methods. You could use
a plain request library, httpx, or another method. You can configure account plugins to handle these differences in authentication methods.

If you want to use different authentication methods, you should create different account plugins. You configure these authentication methods in the `credentials.yaml` file.

Here is an example file:

.. code-block:: sls

    {simple_service_name}.basic_auth:
      default:
        username: my_user
        password: my_pass

Next, you would encrypt the credentials file, and add the encryption key and encrypted file path to the environment.

To encrypt the credentials file, you run the following command:

.. code:: bash

    idem encrypt credentials.yaml

output::

    -A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI=

After you encrypt the credentials file, you add the encryption key and encrypted file path to the environment. You assign the value of the output to `ACCT_KEY` and the value of the path to the encrypted fernet file as `ACCT_FILE`:

.. code:: bash

    export ACCT_KEY="-A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI="
    export ACCT_FILE=$PWD/credentials.yaml.fernet

This enables Idem to read your credentials to authenticate with your cloud provider.

Exec modules
============

Functions placed in `{project_name}/exec/{simple_service_name}/` appear on the hub under `hub.exec.{simple_service_name}.*`. The directory structure under `exec` is arbitrary to Idem, so use it to keep your functions organized.

Do not put all of your functions in one file. The directory structure affects where functions are placed on the hub, and how they are referenced on the CLI.

If you create a function called "get" in `{project_name}/exec/{simple_service_name}/instance`, it can be called from the hub with code:
.. code-block:: python

    hub.exec.simple_service_name.sample.get(name="sample_name", resource_id="resource_1")

It could be called from the Idem CLI like this:

.. code-block:: bash

    idem exec {simple_service_name}.sample.get name=sample_name resource_id=resource_1

The profile you want to use from your encrypted credentials file can be specified on the command line when calling an exec module directly:

.. code:: bash

    idem exec --acct-profile my-staging-env {simple_service_name}.sample.list

The default is to use the profile named "default."

State modules
=============

Here is a sample state in SLS:

.. code:: sls

    ensure_sample_exists:
      {simple_service_name}.sample.present:
        - name: a_sample_name
        - description: Managed by Idem

Here is how you create a sample state:

.. code:: bash

    idem state my_state.sls

Here is how you delete a sample state:

.. code:: bash

    idem state my_state.sls --invert
